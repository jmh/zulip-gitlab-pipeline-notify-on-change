mod pipeline;
//mod trace;

extern crate const_env;
use self::const_env::from_env;
use log::{debug, error, info};
use reqwest;
use serde::Deserialize;
use std::{env, error};
use serde::de::DeserializeOwned;

#[derive(Debug)]
pub struct ApiClient {
    pub api_url: &'static str,
    pub api_token: String,
    previous_commit: Option<String>,
    pub project_id: u32,
    pub current_pipeline: u32,
    pub current_pipeline_status: Option<bool>,
    pub is_target_branch_protected: bool,
    pub author: String,
}

#[from_env("GITLAB_API_KEY")]
const HARD_CODED_API_KEY: &'static str = "";

impl Default for ApiClient {
    fn default() -> ApiClient {
        let a = ApiClient {
            api_url: "https://gitlab.com/api/v4",
            api_token: env::var("GITLAB_API_KEY").unwrap_or(HARD_CODED_API_KEY.to_string()),
            previous_commit: match env::var("CI_COMMIT_BEFORE_SHA")
                .unwrap_or("0000000000000000000000000000000000000000".to_string())
                .as_str()
            {
                "0000000000000000000000000000000000000000" => None,
                a => Some(String::from(a)),
            },
            project_id: env::var("CI_PROJECT_ID")
                .unwrap_or("0".to_string())
                .parse()
                .unwrap(),
            current_pipeline: env::var("CI_PIPELINE_ID")
                .unwrap_or("0".to_string())
                .parse()
                .unwrap(),
            //
            current_pipeline_status: match env::var("CI_JOB_STATUS")
                .unwrap_or("".to_string())
                .as_str()
            {
                "success" => Some(true),
                "failed" => Some(false),
                "cancelled" => {
                    None /* exit */
                }
                _ => None,
            },
            is_target_branch_protected: match env::var("CI_COMMIT_REF_PROTECTED")
                .unwrap_or("false".to_string())
                .as_str()
            {
                "true" => true,
                _ => false,
            },
            author: env::var("GITLAB_USER_EMAIL").unwrap_or("jan@competition.company".to_string()),
        };
        debug!("Gitlab API Client: {:?}", &a);
        a
    }
}

trait Status {
    fn status(&self) -> &str;
    fn id(&self) -> u32;
}

impl Status for PipelineInfo {
    fn status(&self) -> &str {
        self.status.as_str()
    }
    fn id(&self) -> u32 {
        self.id
    }
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct UserInfo {
    id: u32,
    username: String,
    name: String,
    state: String,
    avatar_url: String,
    web_url: String,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct PipelineInfo {
    id: u32,
    iid: u32,
    project_id: u32,
    sha: String,
    /*
    #[serde(rename = "ref")]
    branch: String,
     */
    status: String,
    source: String,
    user: UserInfo,
    before_sha: String,
    //... more things
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct CommitInfo {
    id: String,
    status: String,
}

#[derive(Debug, Deserialize)]
pub enum GLError {
    GitlabError,
    InvalidPreviousPipelineState,
}

impl error::Error for GLError {}
impl std::fmt::Display for GLError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "There is an error: {:?}", self)
    }
}

impl ApiClient {
    pub fn previous_commit_from_pipeline(
        &self,
        pipeline: &u32,
    ) -> Result<String, Box<GLError>> {
        let url = format!(
            "{}/projects/{}/pipelines/{}",
            &self.api_url, &self.project_id, &pipeline
        );
        debug!("GET: {}", &url);

        //let resp: PipelineInfo = tmp_resp.json().expect("JSON PARSE FAILED");

        let resp = self.api::<PipelineInfo>(&url).unwrap();
        info!(
            "Previous sha determined to be: {}",
            &resp.before_sha.to_string()
        );


        Ok(resp.before_sha)
    }

    fn api<T: DeserializeOwned>(&self, url: &str) -> Result<T, GLError> {
        let client = reqwest::blocking::Client::new();
        debug!("GET: {}", &url);
        let tmp_resp = client
            .get(url)
            .header("PRIVATE-TOKEN", &self.api_token)
            .send()
            .expect("Unable to get valid response from API");

        debug!("Response: {:?}", &tmp_resp);
        if &tmp_resp.status().is_success() != &true {
            error!(
                "Response from gitlab api for {} was code {:?}",
                &url,
                &tmp_resp.status()
            );
            return Err(GLError::GitlabError)
        }
        let res_txt = tmp_resp.text().unwrap();
        debug!("Response text: {}", &res_txt);
        debug!("TYPE: {:?}", std::any::type_name::<T>());
        let resp: T = serde_json::from_str(&res_txt).expect("Failed to parse JSON");
        // tmp_resp.json().expect("Failed to Parse JSON");
        Ok(resp)
    }


    #[allow(dead_code)]
    fn retry_api_request<T: DeserializeOwned + Status>(&self, url: &str) -> Result<bool, GLError> {
        loop {
            let resp: T = self.api(&url)?;

            match resp.status() {
                "failed" => return Ok(false),
                "success" => return Ok(true),
                "running" => {
                    error!(
                        "Commit {} is still running a pipeline, assuming that we succeeded",
                        &resp.id()
                    );
                    //FIXME: This should actually retry, we're making a huge assumption here
                    //std::thread::sleep(std::time::Duration::from_secs(1))
                    return Ok(true);
                }
                "pending" => {
                    error!("Commit {} is pending a pipeline to run, retrying", &resp.id());
                    std::thread::sleep(std::time::Duration::from_secs(1))
                }
                _ => {
                    error!("Status: `{}`, looping", resp.status());
                    std::thread::sleep(std::time::Duration::from_secs(1));
                }
            }
        }
    }

    fn get_failed_jobs(&self) -> Option<Vec<u64>> {
        let url = format!(
            "{}/projects/{}/pipelines/{}/jobs",
            &self.api_url, &self.project_id, &self.current_pipeline
        );
        let resp: Vec<pipeline::Job> = self.api(&url).unwrap();
        let mut collector: Vec<u64> = Vec::new();
        for job in resp {
            if job.status == "failed" {
                if job.allow_failure {
                   continue;
                }
                collector.push(job.id);
            }
        }
        if collector.is_empty() { return None }
        Some(collector)
    }

    fn get_one_failed_job(&self) -> Option<u64> {
        match self.get_failed_jobs() {
            Some(a) => Some(a.get(0).unwrap().to_owned()),
            None => None,
        }
    }

    pub fn get_pipeline_status_of_sha(
        &self,
        sha: &String,
    ) -> Result<bool, GLError> {
        let url = format!(
            "https://gitlab.com/api/v4/projects/{}/repository/commits/{}",
            &self.project_id, &sha
        );
        let resp: CommitInfo = self.api(&url)?;
        match resp.status.as_str() {
            "failed" => return Ok(false),
            "success" => return Ok(true),
            _ => {
                error!("Not a valid state for a prior pipeline: {:?}", resp.status);
                Err(GLError::InvalidPreviousPipelineState)
            }
        }
    }

    #[allow(dead_code)]
    pub fn was_previous_pipeline_successful(&self) -> Result<bool, GLError> {
        match &self.previous_commit {
            Some(sha) => self.get_pipeline_status_of_sha(sha),
            None => {
                error!("No `previous` commit in environment, requesting from Gitlab API");
                let old = self
                    .previous_commit_from_pipeline(&self.current_pipeline)
                    .unwrap();
                if old == "0000000000000000000000000000000000000000" {
                    info!("Previous sha is not set, assuming previous build was working");
                    return Ok(true)
                }
                self.get_pipeline_status_of_sha(&old)
            }
        }
    }

    pub fn format_msg(&self, action: bool) -> String {
        // Returns a markdown formatted string with as much information as possible for the build
        // CI_COMMIT_TITLE: conditional run
        // CI_PROJECT_NAME: jans-ci-testing
        // CI_PROJECT_URL: https://gitlab.com/rennsport/mobile/jans-ci-testing
        // URL + '/-/commit/' + CI_COMMIT_SHA -> https://gitlab.com/rennsport/mobile/jans-ci-testing/-/commit/54fea1a69c553b011ab63146e0ee4ab4c5f89f29
        // CI_PIPELINE_URL: https://gitlab.com/rennsport/mobile/jans-ci-testing/-/pipelines/744563089
        // CI_JOB_URL: https://gitlab.com/rennsport/mobile/jans-ci-testing/-/jobs/3588568611
        // CI_BUILD_REF_NAME: main
        let job: String;
        match self.get_one_failed_job() {
            Some(a) => {
                let friendly_job_url = format!("{}/-/jobs/{}", env::var("CI_PROJECT_URL").unwrap(), &a.to_string());
                /*
                let api_job_url = format!(
                    "{}/projects/{}/pipelines/{}/jobs/{}/trace",
                    &self.api_url, &self.project_id, &self.current_pipeline, &a.to_string()
                );
                let job_logs = trace::Job{url: api_job_url.to_string(), api_token: self.api_token.to_string()}
                    .get_job_logs();
                 */

                job = format!("- **Job:**  [{}]({})", &a.to_string(), &friendly_job_url);

            }
            None => job = "".to_string()
        }
        let intro_line: String;
        match action {
            true => {
                intro_line = format!(":green_large_square: Build status for [{}]({}) on {} has been fixed :happy:", env::var("CI_PROJECT_NAME").unwrap(), env::var("CI_PIPELINE_URL").unwrap(), env::var("CI_BUILD_REF_NAME").unwrap());
            },
            false => {
                intro_line = format!(":red_square: Build status for [{}]({}) on {} has broken :sad:", env::var("CI_PROJECT_NAME").unwrap(),env::var("CI_PIPELINE_URL").unwrap(), env::var("CI_BUILD_REF_NAME").unwrap());
            }
        }
        let ci_diff_url = format!("{}/-/commit/{}", env::var("CI_PROJECT_URL").unwrap(), env::var("CI_COMMIT_SHA").unwrap());
        // FIXME: Currently uses it's own job for this
        let pipeline = format!("- **Pipeline**  [{}]({})", env::var("CI_PIPELINE_ID").unwrap(), env::var("CI_PIPELINE_URL").unwrap() );
        let commit = format!("- **Commit Diff:**  [{}]({})", env::var("CI_COMMIT_SHORT_SHA").unwrap(), ci_diff_url);
        [
            intro_line,
            pipeline,
            commit,
            job,
        ].join("\n")
    }

    pub fn have_we_changed_things_for_the_better(&self) -> Option<bool> {
        // Return Something if we need to notify someone
        // Some(false) if we broke things
        // Some(true) if we fixed things
        // None if it's all the same
        /*
        let current = match env::var("RECENT_SUCCESS")
            .unwrap_or("0".to_string())
            .parse()
            .unwrap_or(0)
        {
            1 => true,
            _ => false,
        };
         */
        let current = match self.get_failed_jobs() {
            Some(_) => false,
            None => true,
        };

        /* FIXME: we cannot test our own pipeline as we will be "running"
        let current = gl.was_own_pipeline_successful()
            .expect("Failed to get current pipeline status");
         */

        let previous = self
            .was_previous_pipeline_successful()
            .expect("Failed to get previous pipeline status");

        info!("Previous build succeeded: \x1b[93m{}\x1b[0m", previous);
        info!("Current build succeeded: \x1b[93m{}\x1b[0m", current);

        match (previous, current) {
            (true, false) => Some(false),
            (false, true) => Some(true),
            _ => {
                info!("Looks like everything is the same, not notifying anyone.");
                None
            }
        }
    }
}
