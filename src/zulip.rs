extern crate const_env;
use self::const_env::from_env;
use reqwest;
use serde::Deserialize;
use std::collections::HashMap;
use std::env;
use log::{debug, error, info};


#[from_env("ZULIP_API_KEY")]
const HARD_CODED_API_KEY: &'static str = "";

#[derive(Debug)]
pub struct ApiClient {
    username: &'static str,
    token: String,
    url: &'static str,
}

impl Default for ApiClient {
    fn default() -> ApiClient {
        let a = ApiClient {
            username: "zeta-bot@comco.zulipchat.com",
            token: env::var("ZULIP_API_KEY").unwrap_or(HARD_CODED_API_KEY.to_string()),
            url: "https://comco.zulipchat.com/api/v1",
        };
        debug!("{:?}", &a);
        a
    }
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct UsersResp {
    result: String,
    msg: String,
    user: UserInfo,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct UserInfo {
    email: String,
    full_name: String,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
struct ZulipResponse {
    id: u32,
    result: String,
    msg: String,
}

#[derive(Debug)]
struct ZulipError(String);

impl std::error::Error for ZulipError {}
impl std::fmt::Display for ZulipError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "There is an error: {}", self.0)
    }
}

impl ApiClient {
    fn send_notification(
        &mut self,
        stream: &String,
        topic: Option<&String>,
        message: &String,
    ) -> Result<(), Box<dyn std::error::Error>> {
        // send notification will highlight the target recipient on a topic
        // this method is useful if you want information to be public, like on a CI job against main
        let url = format!("{}/messages", &self.url);
        let client = reqwest::blocking::Client::new();
        let mut params: HashMap<&str, &str> = HashMap::new();
        params.insert("to", &stream);
        if topic.is_some() {
            params.insert("topic", &topic.unwrap());
            params.insert("type", "stream");
        } else {
            params.insert("type", "private");
        }

        params.insert("content", &message);
        debug!("Send notification parameters: {:?}", &params);

        let resp: ZulipResponse = client
            .post(url)
            .basic_auth(&self.username, Some(&self.token))
            .form(&params)
            .send()?
            .json()?;
        info!("Send zulip notification: {:#?}", resp.result);
        match resp.result.as_str() {
            "success" => Ok(()),
            _ => Err(Box::new(ZulipError("Message Send was not success".into()))),
        }
    }

    fn get_name_from_email(&mut self, email_address: &str) -> Option<String> {
        // Zulip "realnames" are not known to gitlab, however we can be reasonably sure you
        // reuse your email address against both Zulip and Gitlab, so we can ask Zulip
        let url = format!("{}/users/{}", &self.url, &email_address);
        let client = reqwest::blocking::Client::new();
        let resp: UsersResp = client
            .get(url)
            .basic_auth(&self.username, Some(&self.token))
            .send()
            .ok()?
            .json()
            .ok()?;
        debug!("Users response: {:#?}", &resp);
        Some(resp.user.full_name)
    }

    pub fn send_notification_to_email(
        &mut self,
        mail: &str,
        message: &str,
        stream: &str,
        topic: &str,
    ) -> bool {
        // Notification means: We will highlight you in a channel.
        // this is more expensive than the direct message way, as we need a real name.
        let name_res = self.get_name_from_email(mail);
        if name_res.is_none() {
            error!("User not found!");
            return false;
        }
        let message = format!("@**{}** {}", &name_res.unwrap(), &message);
        debug!("Message to be sent: {:?}", &message);
        let _ = self.send_notification(
            &stream.to_string(),
            Some(&topic.to_string()),
            &message
        );
        true
    }

    pub fn send_message_to_email(&mut self, mail: &str, message: &str) -> bool {
        let ret_ = self.send_notification(&format!("{}", mail), None, &message.to_string());
        match ret_ {
            Ok(()) => true,
            Err(_e) => false,
        }
    }
}
