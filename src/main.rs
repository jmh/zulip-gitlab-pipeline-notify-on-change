extern crate pretty_env_logger;
#[macro_use]
extern crate log;

mod gitlab;
mod zulip;

fn main() {
    pretty_env_logger::init();
    let gl = gitlab::ApiClient::default();

    let changed_state = gl.have_we_changed_things_for_the_better();

    let action: &str;
    match changed_state {
        Some(false) => action = "busted",
        Some(true) => action = "fixed",
        _ => return,
    }
    info!("{} {} the build", &gl.author, &action);

    let msg = gl.format_msg(changed_state.unwrap());

    let mut z = zulip::ApiClient::default();

    let out: bool;
    if gl.is_target_branch_protected {
        // racing-service / MR #55 feat: add lap number
        let topic = format!(
            "{} / MR #{} {}",
            std::env::var("CI_PROJECT_NAME").unwrap_or("NO_PROJECT".to_string()),
            std::env::var("CI_MERGE_REQUEST_IID").unwrap_or("NO_ID".to_string()),
            std::env::var("CI_MERGE_REQUEST_TITLE").unwrap_or("NO_TITLE".to_string()),
        );
        // t-platform/mr-events
        let stream = "feed/platform/mr-events".to_string();

        info!("Pipeline on a protected branch and build has {}", &action);
        info!(
            "Notifying the author publicly on steam #{} under the topic `{}`",
            &stream, &topic
        );
        out = z.send_notification_to_email(
            &gl.author,
            &format!("Build status became {}\n{}", &action, &msg),
            &stream,
            &topic,
        );
    } else {
        info!("Pipeline has been {} on private branch", &action);
        info!("Notifying the author privately");
        out = z.send_message_to_email(&gl.author, &format!("{} the build\n{}", &action, &msg));
    };
    info!("Author notified: {}", out);

    if changed_state.unwrap() == false {
        error!("Intentionally exiting non-zero as to leave the pipeline API in a \"FAILED\" state");
        std::process::exit(2)
    }
}
