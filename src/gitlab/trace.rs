use serde::Deserialize;

// FIXME: the current API seems to only send the entire fucking log
// however the frontend seems to send JSON that corresponds with these structs. WTF.
// Frontend returns JSON for jobs: https://gitlab.com/rennsport/mobile/jans-ci-testing/-/jobs/3589871407/trace
// API returns a raw multiline ANSI string: https://gitlab.com/api/v4/projects/40119335/jobs/3590529225/trace

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Line {
    // Max offset 65,5536
    pub offset: u16,
    pub content: String,
    pub section: Option<String>,
    pub section_header: Option<bool>,
    pub section_duration: Option<String>,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Trace {
    pub id: u32,
    pub status: String,
    pub complete: bool,
    pub state: String,
    pub append: bool,
    pub truncated: bool,
    pub offset: u16,
    pub size: u16,
    pub total: u16,
    pub lines: Vec<Line>,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Job {
    pub url: String,
    pub api_token: String,
}

impl Job {
    pub fn get_job_logs(&self) -> String {
        let mut ret: Vec<String> = Vec::new();
        let client = reqwest::blocking::Client::new();
        debug!("GET: {}", &self.url);
        let tmp_resp = client
            .get(&self.url)
            .header("PRIVATE-TOKEN", &self.api_token)
            .send()
            .expect("FAILED TO GET A RESPONSE");
        debug!("Response: {:?}", &tmp_resp);
        if &tmp_resp.status().is_success() != &true {
            error!(
                "Response from gitlab api for {} was code {:?}",
                &self.url,
                &tmp_resp.status()
            );
            panic!("Cannot continue without knowing what job")
        };
        let res_text = tmp_resp.text().unwrap();
        debug!("Response from gitlab API: \n{}", &res_text);
        let resp: Vec<Trace> = serde_json::from_str(&res_text).expect("Cannot parse json");
        for trace in resp {
            for lines in trace.lines {
                if lines.section.unwrap_or("no".to_string()) == "step-script" {
                    ret.push(lines.content)
                }
            }
        };
        ret.join("\n")
    }
}