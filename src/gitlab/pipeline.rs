use serde::Deserialize;

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Runner {
    pub id: u64,
    pub description: String,
    pub ip_address: String,
    pub active: bool,
    pub paused: bool,
    pub is_shared: bool,
    pub runner_type: String,
    pub name: String,
    pub online: bool,
    pub status: String,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Artefacts {
    pub file_type: String,
    pub size: u64,
    pub filename: String,
    pub file_format: Option<String>,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Project {
    pub ci_job_token_scope_enabled: bool,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Pipeline {
    pub id: u64,
    pub iid: u64,
    pub project_id: u64,
    pub sha: String,
    #[serde(rename = "ref")]
    pub r#ref: String,
    pub status: String,
    pub source: String,
    pub created_at: String,
    pub updated_at: String,
    pub web_url: String,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Trailers {}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Commit {
    pub id: String,
    pub short_id: String,
    pub created_at: String,
    pub parent_ids: Vec<String>,
    pub title: String,
    pub message: String,
    pub author_name: String,
    pub author_email: String,
    pub authored_date: String,
    pub committer_name: String,
    pub committer_email: String,
    pub committed_date: String,
    pub trailers: Trailers,
    pub web_url: String,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct User {
    pub id: u64,
    pub username: String,
    pub name: String,
    pub state: String,
    pub avatar_url: String,
    pub web_url: String,
    pub bio: String,
    pub location: String,
    pub public_email: Option<String>,
    pub skype: String,
    pub linkedin: String,
    pub twitter: String,
    pub website_url: String,
    pub organization: String,
    pub job_title: String,
    pub pronouns: Option<String>,
    pub bot: bool,
    pub work_information: Option<String>,
    pub local_time: Option<String>,
}

#[allow(dead_code)]
#[derive(Deserialize, Debug)]
pub struct Job {
    pub id: u64,
    pub status: String,
    pub stage: String,
    pub name: String,
    #[serde(rename = "ref")]
    pub r#ref: String,
    pub tag: bool,
    pub coverage: Option<f32>,
    pub allow_failure: bool,
    pub started_at: Option<String>,
    pub finished_at: Option<String>,
    pub duration: Option<f32>,
    pub queued_duration: Option<f32>,
    pub user: User,
    pub commit: Commit,
    pub pipeline: Pipeline,
    pub failure_reason: Option<String>,
    pub web_url: String,
    pub project: Project,
    pub artifacts: Vec<Artefacts>,
    pub runner: Option<Runner>,
    pub artifacts_expire_at: Option<String>,
    pub tag_list: Vec<String>,
}