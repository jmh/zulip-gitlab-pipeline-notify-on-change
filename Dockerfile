FROM rust:latest AS builder
WORKDIR /usr/src/alert
COPY Cargo.toml Cargo.lock ./
RUN cargo fetch
COPY . ./
RUN cargo build --release

FROM busybox:1.35.0-uclibc as busybox


#FROM gcr.io/distroless/base-debian11:latest AS base
FROM gcr.io/distroless/cc:latest AS base
COPY --from=busybox /bin/sh /bin/sh
COPY --from=busybox /bin/mkdir /bin/mkdir
COPY --from=busybox /bin/touch /bin/touch
COPY --from=busybox /bin/cat /bin/cat
LABEL author="Jan Harasym <jan@competition.company>" version="0.1b"
COPY --from=builder /usr/src/alert/target/release/alert-zulip-on-change /alert
CMD /alert